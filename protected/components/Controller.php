<?php
$blog_options=BlogOptions::model()->findAll(array('order'=>'option_name'));
foreach ($blog_options as $option) {
	Yii::app()->params[$option->option_name]=array('value'=>$option->option_value,'show'=>$option->option_show);
}

Yii::app()->name=Yii::app()->params['blog_name']['value'];
if(Yii::app()->params['template']['value']=='box'){Yii::app()->theme='box';}
if(Yii::app()->params['template']['value']=='full'){Yii::app()->theme='full';}
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
}