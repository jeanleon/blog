<?php /* @var $this Controller */ ?>
<?php
	$menu=array(
		array(
			'label'=>'Home',
			'url'=>'#',
			'icon'=>'glyphicon glyphicon-home',
			'items'=>null
		),
		array(
			'label'=>'Menu2',
			'url'=>'menu2',
			'icon'=>'',
			'items'=>array(
				array(
					'label'=>'menu2.1',
					'url'=>'#',
					'icon'=>'',
					'items'=>null
				),
				array(),
				array(
					'label'=>'menu2.2',
					'url'=>'#',
					'icon'=>'',
					'items'=>null
				),
			),
		),
		array(
			'label'=>'Menu3',
			'url'=>'#',
			'items'=>null,
			'icon'=>'glyphicon glyphicon-cog',
		),
	);
	
	$blog_menu=BlogMenus::model()->findByAttributes(array('menu_name'=>Yii::app()->params['menu_primary']['value']));	
	$blog_menu=unserialize($blog_menu->menu_items);	
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="<?php echo Yii::app()->params['blog_language']['value'];?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/bootstrap.css');?>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/box/css/styles.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.bxslider.css">
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-2.1.4.min.js',CClientScript::POS_HEAD);?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/bootstrap.min.js',CClientScript::POS_HEAD);?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.bxslider.js',CClientScript::POS_HEAD);?>
	
	<link rel="icon" type="image/png" href="<?php echo Yii::app()->request->baseUrl.'/images/'.Yii::app()->params['favicon']['value'];?>" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div class="container">
	<?php if(Yii::app()->params['header']['value']){?>
		<?php if(Yii::app()->params['header_top']['value']){?>
			<div class="row top-header">
				<div class="container">
					<div class="hidden-xs hidden-sm col-md-12 col-lg-12">
						<?php if(Yii::app()->params['header_social']['value']){?>
							<div class="col-md-6 col-lg-6">
								<?php 
								$socialicons=BlogSocials::model()->findAll();
								foreach ($socialicons as $icon) {
									if($icon->social_show){
										echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl.'/images/social/'.$icon->social_name.'.png',$icon->social_name,array('width'=>'32px','title'=>$icon->social_data)),$icon->social_link,array('target'=>'_blank'));										
									}																
								}								 
								?>
							</div>
						<?php }?>
						<div class="col-md-6 col-lg-6 pull-right">
							<?php if(Yii::app()->params['header_language']['value']){?>
								<div class="col-md-4 col-lg-3 <?php if(Yii::app()->params['header_search']['value']){echo 'col-lg-push-3';}else{echo 'col-lg-push-10';}?>">
									<?php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl.'/images/ui/spain.png','',array('width'=>'32px','title'=>'Español')),'#');?>
									<?php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl.'/images/ui/usa.png','',array('width'=>'32px','title'=>'English')),'#');?>															
								</div>
							<?php }?>
							<?php if(Yii::app()->params['header_search']['value']){?>
								<div class="col-md-8 col-lg-9 col-lg-push-3 pull-right">
									<form class="form-inline">
										<div class="form-group">
					    					<div class="input-group">
					      						<input type="text" class="form-control" id="buscar" placeholder="Que estas buscando?">
					      						<div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
					    					</div>
					  					</div>		  
									</form>
								</div>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
		<?php }?>
		<div class="row main-header">
			<?php if(Yii::app()->params['header_logo']['show']){?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<?php 
					if(Yii::app()->params['header_logo']['value']){
						echo CHtml::image(Yii::app()->request->baseUrl.'/images/ui/logo.png',Yii::app()->name);
					}
					else{?>
						<img src="http://placehold.it/280x100">
					<?php }?>			
				</div>
			<?php }?>
			<?php if(Yii::app()->params['blog_name']['show']){?>			
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden-xs">
					<div class="page-header">
						<h1>
			  				<?php echo Yii::app()->params['blog_name']['value'];?>
			  				<small class="slogan"><?php echo Yii::app()->params['blog_description']['value'];?></small>
			  			</h1>
			  		</div>			
				</div>			
			</div>
			<?php }?>
			<?php if(Yii::app()->params['header_ads']['show']){?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pull-right">
					<ul class="bxslider">
  						<a href="www.google.com"><li><img alt="Mi sitio" src="/blog/images/ui/logo.png"></li></a>  
  						<li><img src="http://placehold.it/280x100"></li>
  						<li><img src="http://placehold.it/280x100"></li>
  						<li><img src="http://placehold.it/280x100"></li>
					</ul>		
						  	
				</div>
			<?php }?>			
		</div>
	<?php }?>
	<div class="row">
		<nav class="navbar navbar-default">
	  		<div class="container-fluid">
	    		<div class="navbar-header">
	      			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        			<span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				     </button>				     
	    		</div>
	
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    	<ul class="nav navbar-nav">
			    		<?php			    		
			    		foreach ($blog_menu as $menu) {			    			
							if(is_array($menu)){								
								if(is_array($menu['items'])){
									echo '<li class="dropdown">';
									echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">';
									echo '<i class="'.$menu['icon'].'"></i> '.$menu['label'].'<span class="caret"></span></a>';									
									echo '<ul class="dropdown-menu" role="menu">';	
									foreach ($menu['items'] as $submenu) {
										if(empty($submenu)){
											echo '<li class="divider"></li>';
										}
										else{
											echo '<li><a href="'.$submenu['url'].'">'.$submenu['label'].'</a></li>';
										}
									}
									echo '</ul></li>';
								}
								else{
									echo '<li><a href="'.$menu['url'].'"><i class="'.$menu['icon'].'"></i> '.$menu['label'].'</a></li>';
								}
							}
							else{
								
							}				
						}
			    		?>
			      	</ul>
	      			<ul class="nav navbar-nav navbar-right">
	        			<li><a href="#">Link</a></li>
	        			<li><a href="#" data-toggle="modal" data-target="#login">Entrar</a></li>
	        			<li class="dropdown">
	          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Iniciar sesion <span class="caret"></span></a>
	          				<ul class="dropdown-menu" role="menu">
	            				<li><a href="#">Another action</a></li>
					            <li><a href="#">Something else here</a></li>
					            <li class="divider"></li>
					            <li><a href="#">Separated link</a></li>
	          				</ul>
	        			</li>
	      			</ul>
	    		</div><!-- /.navbar-collapse -->
	  		</div><!-- /.container-fluid -->
		</nav>
	</div>
	<div class="row">
		<?php if(isset($this->breadcrumbs)):?>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
			)); ?><!-- breadcrumbs -->
		<?php endif?>

		<?php echo $content; ?>	
	</div>
	<div class="clear"></div>
	<div class="row">
		<div id="footer">
			Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
			All Rights Reserved.<br/>
			<?php echo Yii::powered(); ?>
		</div>		
	</div>
</div>



<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="exampleModalLabel">Iniciar sesion</h4>
      		</div>
      		<div class="modal-body">
        		<form>
          			<div class="form-group">
            			<label for="recipient-name" class="control-label">Usuario:</label>
            			<input type="text" class="form-control" id="user">
          			</div>
          			<div class="form-group">
            			<label for="message-text" class="control-label">Password:</label>
            			<input type="text" class="form-control" id="password">            			
          			</div>
        		</form>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-primary">Entrar</button>
      		</div>
    	</div>
  	</div>
</div>

<script>
	$(document).ready(function(){  		
  		$('.bxslider').bxSlider({  			
  			pager:false,
  			controls:false,
  			auto:true,  			  			
		});
	});
</script>
</body>
</html>